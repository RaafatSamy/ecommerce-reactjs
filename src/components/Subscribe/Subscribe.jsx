
const bannerStyle = {
  background: "linear-gradient(to right, #FFA500, #FF8C00)",
  backgroundPosition: "center",
  backgroundRepeat: "no-repeat",
  height: "100%",
  width: "100%",
}

const Subscribe = () => {
  return (
    <div 
      data-aos="zoom-in"
      className="mb-20 bg-gray-100 dark:bg-gray-800 text-white"
      style={bannerStyle}
    >
      <div className="container backdrop-blur-sm py-10">
       <div className="space-y-6 max-w-xl mx-auto">
          <h1 className="text-sxl !text-center sm:text-left sm:text-4xl font-semibold">Get Notified About New Products</h1>
          <input type="text" placeholder="Enter your email"  className="w-full p-3"/>
       </div>
      </div>
    </div>
  )
}

export default Subscribe
